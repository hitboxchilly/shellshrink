# shellshrink

Python 3

Contract strings in the opposite manner of shell expansion.

## Motivation

Reduce long logging strings documenting file renaming.

## Problem

This improperly shinks the likes of 'file.1.ext.2' 'file.3.ext.4' to
'file.{1,3}.ext.{2,4}

Expanding (reverse shrink) that result would result in four strings
'file.1.ext.2' 'file.1.ext.4' 'file.3.ext.2' 'file.3.ext.4'

For proper expansion it should be 'file.{1.ext.2,3.ext.4}'
