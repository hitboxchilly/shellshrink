from setuptools import setup

setup(
    name = 'shellshrink',
    version = '0.1',
    description = 'The opposite of shell expansion for strings.',
    py_modules = ['shellshrink']
)
