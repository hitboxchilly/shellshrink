import argparse

from itertools import zip_longest

def shellshrink(s1, s2):
    """
    Return the opposite of shell expansion between two strings.

    e.g.: 'file.1', 'file.2' -> 'file.{1,2}'
    """
    s = ''
    i = zip_longest(s1, s2)
    def none2empty(x):
        return x if x is not None else ''
    while True:
        try:
            c1, c2 = next(i)
        except StopIteration:
            break
        if c1 == c2:
            s += c1
        else:
            s += '{'
            l = none2empty(c1)
            r = none2empty(c2)
            after = ''
            while c1 != c2:
                try:
                    c1, c2 = next(i)
                except StopIteration:
                    break
                if c1 == c2:
                    after = c1
                else:
                    l += none2empty(c1)
                    r += none2empty(c2)
            s += f'{l},{r}'
            s += '}'
            s += after
    return s

def main(argv=None):
    parser = argparse.ArgumentParser(description=shellshrink.__doc__, prog='shellshrink')
    parser.add_argument('s1', help='First string')
    parser.add_argument('s2', help='Second string')
    args = parser.parse_args(argv)
    print(shellshrink(args.s1, args.s2))

if __name__ == '__main__':
    main()
