import unittest

from shellshrink import shellshrink

class TestShellShrink(unittest.TestCase):

    def test_shellshrink_extension(self):
        self.assertEqual(shellshrink('file.1', 'file.2'), 'file.{1,2}')

    def test_shellshrink_middle(self):
        self.assertEqual(shellshrink('file.1.ext', 'file.2.ext'), 'file.{1,2}.ext')

    def test_shellshrink_nocommon(self):
        self.assertEqual(shellshrink('abc.1', 'abc'), 'abc{.1,}')
        self.assertEqual(shellshrink('abc', 'abc.1'), 'abc{,.1}')

    def test_shellshrink_unique(self):
        self.assertEqual(shellshrink('abc', 'def'), '{abc,def}')

    def test_improper_behavior(self):
        # might as well test for the improper behavior
        self.assertEqual(shellshrink('file.1.ext.2', 'file.3.ext.4'), 'file.{1,3}.ext.{2,4}')


if __name__ == '__main__':
    unittest.main()
